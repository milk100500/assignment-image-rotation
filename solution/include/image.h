#ifndef IMAGE
#define IMAGE

#include <stdint.h>

struct  __attribute__((__packed__)) pixel {
        uint8_t b;
        uint8_t g;
        uint8_t r;
};

struct image {
        uint64_t width;
        uint64_t height;
        struct pixel *data;
};

struct image image_allocate(uint32_t height, uint32_t width);

void image_free(struct image *img);

void pixel_set(struct image *img, uint32_t x, uint32_t y, struct pixel pixel);

#endif
