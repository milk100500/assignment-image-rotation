#include "transform.h"

enum image_transform_status image_rotate_90(struct image const img_in, struct image *img_out) {
        *img_out = image_allocate(img_in.width, img_in.height);
        if (!img_out->data)
                return TRANSFORM_OUT_OF_MEMORY;

        for(uint32_t y = 0; y < img_in.height; ++y) {
                for (uint32_t x = 0; x < img_in.width; ++x) {
                        pixel_set(img_out,
                                  img_in.height - 1 - y,
                                  x,
                                  img_in.data[(img_in.width) * y + x]);
                }
        }
        return TRANSFORM_OK;
}
