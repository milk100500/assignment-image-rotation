#include "transform.h"

#define ARGS_ERROR_MESSAGES_COUNT 5

enum args_status {
        ARGS_OK = 0,
        ARGS_NOT_ENOUGH,
        ARGS_TOO_MANY,
        ARGS_INVALID_INPUT,
        ARGS_INVALID_OUTPUT,
};

char *args_error_messages[ARGS_ERROR_MESSAGES_COUNT] = {
        [ARGS_NOT_ENOUGH] = "not enough arguments",
        [ARGS_TOO_MANY] = "too many arguments",
        [ARGS_INVALID_INPUT] = "input file opening failed",
        [ARGS_INVALID_OUTPUT] = "output file opening failed",
};

char* image_read_error_messages[IMAGE_READ_ERROR_MESSAGES_COUNT] = {
        [READ_INVALID_HEADER] = "header reading failed",
        [READ_OUT_OF_MEMORY] = "image allocation failed",
        [READ_INVALID_OFFSET] = "offset skipping failed",
        [READ_INVALID_PIXELS] = "pixels reading failed",
        [READ_INVALID_PADDING] = "padding skipping failed",
};

char* image_transform_error_messages[IMAGE_TRANSFORM_ERROR_MESSAGES_COUNT] = {
        [TRANSFORM_OUT_OF_MEMORY] = "image allocation failed",
};

char* image_write_error_messages[IMAGE_WRITE_ERROR_MESSAGES_COUNT] = {
        [WRITE_INVALID_HEADER] ="header writing failed",
        [WRITE_INVALID_PIXELS] = "while writing pixels failed",
};


static inline enum args_status args_validate(int argc, char **argv, FILE **fin, FILE **fout)
{
        if (argc < 3)
                return ARGS_NOT_ENOUGH;
        if (argc > 3)
                return ARGS_TOO_MANY;

        *fin = fopen(argv[1], "rb");
        if (!*fin)
                return ARGS_INVALID_INPUT;
        *fout = fopen(argv[2], "wb");
        if (!*fout) {
                fclose(*fin);
                return ARGS_INVALID_OUTPUT;
        }

        return ARGS_OK;
}

int main(int argc, char **argv)
{
        FILE* fin;
        FILE* fout;
        struct image img_in;
        struct image img_out;
        int status;

        enum args_status astatus = args_validate(argc, argv, &fin, &fout);
        if (astatus != ARGS_OK) {
                printf("Arguments validation failed: %s\n", args_error_messages[astatus]);
                return 1;
        }

        status = 0;

        enum image_read_status rstatus = from_bmp(fin, &img_in);
        if (rstatus != READ_OK) {
                printf("Image reading failed: %s\n", image_read_error_messages[rstatus]);
                status = 2;
                goto close_files;
        }

        enum image_transform_status tstatus = image_rotate_90(img_in, &img_out);
        if (tstatus != TRANSFORM_OK) {
                printf("Image transformation failed: %s\n", image_transform_error_messages[tstatus]);
                image_free(&img_in);
                status = 3;
                goto close_files;
        }
        image_free(&img_in);

        enum image_write_status wstatus = to_bmp(fout, &img_out);
        if (wstatus != WRITE_OK) {
                printf("Image writing failed: %s\n", image_write_error_messages[wstatus]);
                status = 4;
        }
        image_free(&img_out);

close_files:
        fclose(fin);
        fclose(fout);

        return status;
}
