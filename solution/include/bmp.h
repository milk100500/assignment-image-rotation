#ifndef BMP
#define BMP

#include <stdio.h>
#include "image.h"

#define IMAGE_READ_ERROR_MESSAGES_COUNT 6
#define IMAGE_TRANSFORM_ERROR_MESSAGES_COUNT 2
#define IMAGE_WRITE_ERROR_MESSAGES_COUNT 3

enum image_read_status {
        READ_OK = 0,
        READ_INVALID_HEADER,
        READ_OUT_OF_MEMORY,
        READ_INVALID_OFFSET,
        READ_INVALID_PADDING,
        READ_INVALID_PIXELS,
};

enum image_transform_status {
        TRANSFORM_OK = 0,
        TRANSFORM_OUT_OF_MEMORY,
};

enum image_write_status {
        WRITE_OK = 0,
        WRITE_INVALID_HEADER,
        WRITE_INVALID_PIXELS,
};

enum image_read_status from_bmp(FILE *in, struct image *img);

enum image_write_status to_bmp(FILE *out, struct image const *img);

#endif
