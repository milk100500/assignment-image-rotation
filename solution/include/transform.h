#ifndef TRANSFORM
#define TRANSFORM

#include "bmp.h"
#include "image.h"

enum image_transform_status image_rotate_90(struct image const img_in, struct image *img_out);

#endif
