#include "bmp.h"
#include <inttypes.h>

#define BMP_SIGNATURE 0x4D42    /* hex of "BM" - BMP format specification */
#define BIT_PER_COLOR 24
#define PLANES 1
#define PIXEL_PER_M 2834

struct __attribute__((__packed__)) bmp_header {
        uint16_t bfType;
        uint32_t bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t biHeight;
        uint16_t biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

static inline uint32_t bmp_padding_calculate(uint32_t image_width)
{
        return image_width % 4;
}

static inline size_t bmp_header_read(FILE *in, struct bmp_header *header)
{
        return fread(header, sizeof(struct bmp_header), 1, in);
}

static inline int bmp_offset_seek(FILE *in, uint32_t offset)
{
        return fseek(in, offset, SEEK_SET);
}

static inline size_t bmp_body_readrow(FILE *in, struct image *img, uint32_t row)
{
        return fread(img->data + img->width * row, sizeof(struct pixel), img->width, in);
}

static inline int bmp_padding_skip(FILE *in, uint32_t padding)
{
        return fseek(in, padding, SEEK_CUR);
}

static inline enum image_read_status bmp_body_read(FILE *in, struct image *img)
{
        uint32_t padding;

        padding = bmp_padding_calculate(img->width);
        for (uint32_t y = 0; y < img->height; y++) {
                if (bmp_body_readrow(in, img, y) != img->width)
                        return READ_INVALID_PIXELS;

                if (bmp_padding_skip(in, padding) != 0)
                        return READ_INVALID_PADDING;
        }
        return READ_OK;
}

enum image_read_status from_bmp(FILE *in, struct image *img)
{
        struct bmp_header header;
        enum image_read_status rstatus;

        if (bmp_header_read(in, &header) < 1 )
                return READ_INVALID_HEADER;

        *img = image_allocate(header.biHeight, header.biWidth);
        if (img->data == NULL)
                return READ_OUT_OF_MEMORY;

        if (bmp_offset_seek(in, header.bOffBits) != 0) {
                image_free(img);
                return READ_INVALID_OFFSET;
        }

        rstatus = bmp_body_read(in, img);
        if (rstatus != READ_OK) {
                image_free(img);
                return rstatus;
        }

        return READ_OK;
}

static inline size_t bmp_header_write(FILE *out, struct bmp_header *header)
{
        return fwrite(header, sizeof(struct bmp_header), 1, out);
}

static inline struct bmp_header bmp_header_create(uint32_t height, uint32_t width, size_t padding)
{
        size_t image_size;

        image_size = height * (width + padding) * sizeof(struct pixel);
        return (struct bmp_header) {
                .bfType = BMP_SIGNATURE,
                .biHeight = height,
                .biWidth = width,
                .bOffBits = sizeof(struct bmp_header),
                .biSize = 40,
                .biPlanes = PLANES,
                .bfileSize = sizeof(struct bmp_header) + image_size,
                .biCompression = 0,
                .bfReserved = 0,
                .biBitCount = BIT_PER_COLOR,
                .biXPelsPerMeter = PIXEL_PER_M,
                .biYPelsPerMeter = PIXEL_PER_M,
                .biClrUsed = 0,
                .biClrImportant = 0,
                .biSizeImage = image_size
        };
}

static inline size_t bmp_body_writerow(FILE *out, struct image const *img, uint32_t row)
{
        return fwrite(&img->data[row * img->width],
                      sizeof(struct pixel),
                              img->width, out);
}

enum image_write_status to_bmp(FILE *out, struct image const *img)
{
        struct bmp_header header;
        size_t padding;

        padding = bmp_padding_calculate(img->width);

        header = bmp_header_create(img->height, img->width, padding);
        if (bmp_header_write(out, &header) < 1)
                return WRITE_INVALID_HEADER;

        for (uint32_t row = 0; row < img->height; ++row) {
                if (bmp_body_writerow(out, img, row) != img->width)
                        return WRITE_INVALID_PIXELS;

                for (size_t i=0; i < padding; ++i) {
                        putc(0, out);
                }
        }
        return WRITE_OK;
}
