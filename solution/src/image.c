#include "image.h"
#include <malloc.h>

struct image image_allocate(uint32_t height, uint32_t width)
{
        return (struct image) {
                .height = height,
                .width = width,
                .data =malloc(height * width * sizeof(struct pixel))
        };
}

void image_free(struct image *img)
{
        if (img && img->data)
                free(img->data);
}

void pixel_set(struct image *img, uint32_t x, uint32_t y, struct pixel pixel)
{
        if (img)
                img->data[img->width * y + x] = pixel;
}
